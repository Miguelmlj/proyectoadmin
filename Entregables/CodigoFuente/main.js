$(document).ready(function() {

    var circulosVisiblesNivel3 = 0;
    var circulosVisiblesNivel2 = 0;
    var circulosVisiblesNivel1 = 0;
    var barrasVisiblesNivel2 = 0;
    var barrasVisiblesNivel1 = 0;
    var ValorNumeroArabigo = 0;
    var nivelTresKey = 0;
    var nivelDosKeyBolas = 0;
    var nivelDosKeyBarras = 0;
    var nivelUnoKeyBolas = 0;
    var nivelUnoKeyBarras = 0;

    
    //Tutorial
$('#botonForm').on('click', function() {
    
    $(".modal-header").css("background-color", "#50575A");
    $(".modal-header").css("color", "black");
    $(".modal-title").text("Tutorial Introductorio");
    $("#modalTutorial").modal("show");
    
});
    
    //CONTROLES PARA NIVEL 3
    //circulos
    
    $('#botonControlCirculoIzq_Superior').on('click', function() {
        
        if((nivelUnoKeyBolas == 0) && (nivelUnoKeyBarras == 0) && (nivelDosKeyBolas == 0) && (nivelDosKeyBarras == 0)){ //llave para activar número 400
        if (circulosVisiblesNivel3 == 1) {

            $("#circulo1Nivel3").css('visibility', 'hidden');
            //función add
            $("#imgCero3").css('visibility', 'visible');
            circulosVisiblesNivel3 = 0;
            ValorNumeroArabigo-=400;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            nivelTresKey = 0;
        }
        }
        

    });

    $('#botonControlCirculoDer_Superior').on('click', function() {

        if((nivelUnoKeyBolas == 0) && (nivelUnoKeyBarras == 0) && (nivelDosKeyBolas == 0) && (nivelDosKeyBarras == 0)){    
        if (circulosVisiblesNivel3 == 0) {

            $('#circulo1Nivel3').css('visibility', 'visible');
            //función add
            $("#imgCero3").css('visibility', 'hidden');
            circulosVisiblesNivel3 = 1;            
            ValorNumeroArabigo+=400;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            nivelTresKey = 1;
        }
        }

    });
        

    //CONTROLES PARA NIVEL 2
    //circulos
    
    $('#botonControlCirculoIzq_Medio').on('click', function() {
        if(nivelTresKey == 0){
        if (circulosVisiblesNivel2 == 4) {
            
            $("#circulo4Nivel2").css('visibility', 'hidden');
            circulosVisiblesNivel2 = 3;
            ValorNumeroArabigo-=20;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            
            
        } else if (circulosVisiblesNivel2 == 3) {

            $("#circulo1Nivel2").css('visibility', 'hidden');
            circulosVisiblesNivel2 = 2;
            ValorNumeroArabigo-=20;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            
            
        } else if (circulosVisiblesNivel2 == 2) {

            $("#circulo3Nivel2").css('visibility', 'hidden');
            circulosVisiblesNivel2 = 1;
            ValorNumeroArabigo-=20;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            
            
        } else if (circulosVisiblesNivel2 == 1) {

            $("#circulo2Nivel2").css('visibility', 'hidden');
            circulosVisiblesNivel2 = 0;
            ValorNumeroArabigo-=20;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            nivelDosKeyBolas = 0;
            
            if(barrasVisiblesNivel2 == 0){
                //función add
            $("#imgCero2").css('visibility', 'visible');
            }
            
        }
        }

    });

    $('#botonControlCirculoDer_Medio').on('click', function() {

        if(nivelTresKey == 0){
        if (circulosVisiblesNivel2 == 0) {

            $("#circulo2Nivel2").css('visibility', 'visible');
            //función add
            $("#imgCero2").css('visibility', 'hidden');
            
            circulosVisiblesNivel2 = 1;
            ValorNumeroArabigo+=20;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            nivelDosKeyBolas = 1;
            
        } else if (circulosVisiblesNivel2 == 1) {

            $("#circulo3Nivel2").css('visibility', 'visible');
            circulosVisiblesNivel2 = 2;
            ValorNumeroArabigo+=20;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            
        } else if (circulosVisiblesNivel2 == 2) {

            $("#circulo1Nivel2").css('visibility', 'visible');
            circulosVisiblesNivel2 = 3;
            ValorNumeroArabigo+=20;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            
        } else if (circulosVisiblesNivel2 == 3) {

            $("#circulo4Nivel2").css('visibility', 'visible');
            circulosVisiblesNivel2 = 4;
            ValorNumeroArabigo+=20;
            $("#numeroArabigo").html(ValorNumeroArabigo);
        }
        }    

    });

    //barras    
    $('#botonControlBarraIzq_Medio').on('click', function() {
        
        if(nivelTresKey == 0){
        if (barrasVisiblesNivel2 == 3) {
            $("#barra3Nivel2").css('visibility', 'hidden');
            barrasVisiblesNivel2 = 2;
            ValorNumeroArabigo-=100;
            $("#numeroArabigo").html(ValorNumeroArabigo);

        } else if (barrasVisiblesNivel2 == 2) {
            $("#barra2Nivel2").css('visibility', 'hidden');
            barrasVisiblesNivel2 = 1;
            ValorNumeroArabigo-=100;
            $("#numeroArabigo").html(ValorNumeroArabigo);

        } else if (barrasVisiblesNivel2 == 1) {
            $("#barra1Nivel2").css('visibility', 'hidden');
            barrasVisiblesNivel2 = 0;
            ValorNumeroArabigo-=100;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            nivelDosKeyBarras = 0;
            if(circulosVisiblesNivel2 == 0){
                //función add
            $("#imgCero2").css('visibility', 'visible');
            }
            
        }
        }    

    });

    $('#botonControlBarraDer_Medio').on('click', function() {

        if(nivelTresKey == 0){
        if (barrasVisiblesNivel2 == 0) {
            $("#barra1Nivel2").css('visibility', 'visible');
            //función add
            $("#imgCero2").css('visibility', 'hidden');
            
            barrasVisiblesNivel2 = 1;
            ValorNumeroArabigo+=100;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            nivelDosKeyBarras = 1;
                
        } else if (barrasVisiblesNivel2 == 1) {
            $("#barra2Nivel2").css('visibility', 'visible');
            barrasVisiblesNivel2 = 2;
            ValorNumeroArabigo+=100;
            $("#numeroArabigo").html(ValorNumeroArabigo);

        } else if (barrasVisiblesNivel2 == 2) {
            $("#barra3Nivel2").css('visibility', 'visible');
            barrasVisiblesNivel2 = 3;
            ValorNumeroArabigo+=100;
            $("#numeroArabigo").html(ValorNumeroArabigo);

        }
        }

    });

    //CONTROLES PARA NIVEL 1

    $('#botonControlCirculoIzq_Inferior').on('click', function() {
        if(nivelTresKey == 0){
        if (circulosVisiblesNivel1 == 4) {

            $("#circulo4Nivel1").css('visibility', 'hidden');
            circulosVisiblesNivel1 = 3;
            ValorNumeroArabigo-=1;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            
        } else if (circulosVisiblesNivel1 == 3) {

            $("#circulo1Nivel1").css('visibility', 'hidden');
            circulosVisiblesNivel1 = 2;
            ValorNumeroArabigo-=1;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            
        } else if (circulosVisiblesNivel1 == 2) {

            $("#circulo3Nivel1").css('visibility', 'hidden');
            circulosVisiblesNivel1 = 1;
            ValorNumeroArabigo-=1;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            
        } else if (circulosVisiblesNivel1 == 1) {

            $("#circulo2Nivel1").css('visibility', 'hidden');
            circulosVisiblesNivel1 = 0;
            ValorNumeroArabigo-=1;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            nivelUnoKeyBolas = 0;
            
            if(barrasVisiblesNivel1 == 0){
                //función add
            $("#imgCero1").css('visibility', 'visible');
            }
            
        }
        }    

    });

    $('#botonControlCirculoDer_Inferior').on('click', function() {
        
        if(nivelTresKey == 0){
        if (circulosVisiblesNivel1 == 0) {

            $("#circulo2Nivel1").css('visibility', 'visible');
            //función add
            $("#imgCero1").css('visibility', 'hidden');
            
            circulosVisiblesNivel1 = 1;
            ValorNumeroArabigo+=1;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            nivelUnoKeyBolas = 1;
        
        } else if (circulosVisiblesNivel1 == 1) {

            $("#circulo3Nivel1").css('visibility', 'visible');
            circulosVisiblesNivel1 = 2;
            ValorNumeroArabigo+=1;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            
        } else if (circulosVisiblesNivel1 == 2) {

            $("#circulo1Nivel1").css('visibility', 'visible');
            circulosVisiblesNivel1 = 3;
            ValorNumeroArabigo+=1;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            
        } else if (circulosVisiblesNivel1 == 3) {

            $("#circulo4Nivel1").css('visibility', 'visible');
            circulosVisiblesNivel1 = 4;
            ValorNumeroArabigo+=1;
            $("#numeroArabigo").html(ValorNumeroArabigo);
        }
        }

    });

    //barras
    $('#botonControlBarraIzq_Inferior').on('click', function() {
        
        if(nivelTresKey == 0){
        if (barrasVisiblesNivel1 == 3) {
            $("#barra3Nivel1").css('visibility', 'hidden');
            barrasVisiblesNivel1 = 2;
            ValorNumeroArabigo-=5;
            $("#numeroArabigo").html(ValorNumeroArabigo);

        } else if (barrasVisiblesNivel1 == 2) {
            $("#barra2Nivel1").css('visibility', 'hidden');
            barrasVisiblesNivel1 = 1;
            ValorNumeroArabigo-=5;
            $("#numeroArabigo").html(ValorNumeroArabigo);

        } else if (barrasVisiblesNivel1 == 1) {
            $("#barra1Nivel1").css('visibility', 'hidden');
            barrasVisiblesNivel1 = 0;
            ValorNumeroArabigo-=5;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            nivelUnoKeyBarras = 0;
            if(circulosVisiblesNivel2 == 0){
                //función add
            $("#imgCero1").css('visibility', 'visible');
            }
            
        }
        }

    });

    $('#botonControlBarraDer_Inferior').on('click', function() {
        
        if(nivelTresKey == 0){
        if (barrasVisiblesNivel1 == 0) {
            $("#barra1Nivel1").css('visibility', 'visible');
            //función add
            $("#imgCero1").css('visibility', 'hidden');
            
            barrasVisiblesNivel1 = 1;
            ValorNumeroArabigo+=5;
            $("#numeroArabigo").html(ValorNumeroArabigo);
            nivelUnoKeyBarras = 1;

        } else if (barrasVisiblesNivel1 == 1) {
            $("#barra2Nivel1").css('visibility', 'visible');
            barrasVisiblesNivel1 = 2;
            ValorNumeroArabigo+=5;
            $("#numeroArabigo").html(ValorNumeroArabigo);

        } else if (barrasVisiblesNivel1 == 2) {
            $("#barra3Nivel1").css('visibility', 'visible');
            barrasVisiblesNivel1 = 3;
            ValorNumeroArabigo+=5;
            $("#numeroArabigo").html(ValorNumeroArabigo);

        }
        }

    });
    
});